const config = require("./config.json");

var request = require('request'),
	fs = require('fs'),
	auth = "Basic " + new Buffer(config.username + ":" + config.password).toString("base64");
	
var resultArray = [];
	
let pageUrls = [];
request(
	{
		url : config.url,
		rejectUnauthorized: false,
		headers: {
			"Authorization": auth
		}
	},
	(error, response, body) => {
		let patt = /href="https:\/\/(.*?)"/g;
		while (match = patt.exec(body)) {
			let urlMatch = match[0].substring(6, match[0].length - 1); // Remove href=" and trailing "
			if (pageUrls.indexOf(urlMatch == -1)) { // Remove anchors & deduplicate
				pageUrls.push(urlMatch);
			}
		}
		
		console.log('Requesting ' + pageUrls.length + ' urls.');
		let paramArray = [];
		for(key in pageUrls) {
			urlParams = {
				url : pageUrls[key],
				rejectUnauthorized: false,
				headers: {
					"Authorization": auth
				}	
			};
			paramArray.push(urlParams);
		}
		urlTime(paramArray);
	}
);


function urlTime(paramArray) {
	if (!paramArray.length) { 
		saveResults();
		return true;
	}
	reqParams = paramArray.shift();
	let startTime = new Date().getTime();
	request(
		reqParams,
		(error, response, body) => {
			let endTime = new Date().getTime();
			let elapsed = endTime - startTime;
			let time = parseInt(elapsed);
			
			let color = '';
			if (time > 1200) {
				color = "\x1b[31m";
			} else if (time > 900) {
				 color = "\x1b[33m";
			} else {
				color = "\x1b[32m";
			}
			console.log(color, "[" + elapsed + "ms] " + reqParams.url);
			resultArray.push({url: reqParams.url, elapsed: elapsed});
			urlTime(paramArray);
		}
	);
}

function saveResults() {
	let fileName = 'results/' + new Date().getTime() + '.json';
	fs.writeFileSync(fileName, JSON.stringify(resultArray)); 
}